
---
# DevOps report CA5 - Part 1

## Jenkins

### Install Jenkins

The first step to take is to install Jenkins.

So, go to https://jenkins.io/download/ and download the .war file.

Then go to command line, to the directory where the .war file is and run

    $java -jar jenkins.war

By default, it will run on port 8080, if you want to select other, do

    $java -jar jenkins.war --httpPort=9090

Now go to the selected port on your browser and follow the initial configurations.

### Create the Jenkinsfile

Create a new file name 'Jenkinsfile' on the root folder.

This file will be divided in 4 stages: Checkout, Build, Test and Push.

The file should look something like the following, making sure on Checkout stage you set the correct git, and the correct directory on the following stages.


    pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/david1211758/devops-21-22-lmn-1211758'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                dir ('CA5/gradle_basic_demo'){
                    bat './gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ('CA5/gradle_basic_demo'){
                    bat './gradlew test'
                }
            }
        }
        stage('Archiving') {
               steps {
                    echo 'Archiving...'
                    dir ('CA5/gradle_basic_demo'){
                    archiveArtifacts 'build/distributions/*'
                    }
                }
        }
    }
    }

### Build your item

In Jenkins, which should be running on the port previously selected, click in 'New Item'

Next give a name and select 'Pipeline'.

On the next page, at the bottom in Pipeline, on Definition select 'Pipeline script from SCM'.

Now save.

Your pipeline must have been built now. Click on the build number and check 'Console Output' to see if anything went wrong.

That is a great place to see what is going on with your build and which part may be failing.

If everything went well, in Stage View you should see everything (all 4 stages) colored green.

### Publish in Jenkins the test results

To make our pipeline publish the test results, we need to add a new section to the Jenkinsfile.

At the end of the file, add a post  function

    post{
        always {
            junit 'CA5/gradle_basic_demo/build/test-results/test/*.xml'
              }
        }

And if everything went according to the plan, your Pipeline Jenkins page should look similar to:

![img.png](img.png)






# DevOps report CA5 - Part 2

## Jenkins - Create a pipeline to build the tutorial spring boot application from CA2 part 2

### Prepare files and dir

Firstly, let's create a new folder in CA5, for example /CA5/Part2

And then copy into that folder the gradle_basic_demo folder from the previous exercise, a Dockerfile and a Jenkinsfile.

### Install plugins

For this exercise, we will need a couple of plug ins in your Jenkins, so open Jenkins in your browser using the previously set port and install:

HTMLPublisher
Docker Pipeline

### Docker credentials

Since we are managing Jenkins, let's get our Docker credentials set and ready to use.

Go to Manage Jenkins, Manage Credentials, Add Credentials. Now set up your credentials using your login and password from your Docker account.

Save the ID, we will use it later.

### New Jenkinsfile

On this Jenkinsfile, for Part2, we will have 2 additional stages on our file.

One to create Java Docs and another one to generate a Docker imagewith Tomcat and publish it to Docker Hub.

So, after completing all stages we should have something like:

    pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/david1211758/devops-21-22-lmn-1211758'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                dir ('CA5/Part2/gradle_basic_demo'){
                    bat './gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ('CA5/Part2/gradle_basic_demo'){
                    bat './gradlew test'
                }
            }
        }
        stage('Java Doc') {
            steps {
                echo 'Creating Javadoc...'
                dir ('CA5/Part2/gradle_basic_demo'){
                    bat './gradlew javadoc'
                }

            }
        }
        stage('Archiving') {
               steps {
                    echo 'Archiving...'
                    dir ('CA5/Part2/gradle_basic_demo'){
                    archiveArtifacts 'build/distributions/*'
                    }
                }
        }
        stage('Docker Image') {
               steps {
                    echo 'creating...'
                    dir('CA5/Part2') {
                    script {
                    docker.withRegistry('https://registry.hub.docker.com', 'DockerCredentials') {

                                    def image = docker.build("davidtg/ca5part2:${env.BUILD_ID}")
                                    image.push()
                    }
                    }
                }
        }
    }
    }
        post{
            always {
                dir('CA5/Part2/gradle_basic_demo'){
                junit 'build/test-results/test/*.xml'
                }
                    publishHTML target: [
                            allowMissing: false,
                            alwaysLinkToLastBuild: false,
                            keepAll: true,
                            reportDir: 'CA5/Part2/gradle_basic_demo/build/docs/javadoc',
                            reportFiles: 'index.html',
                            reportName: 'RCov Report'
                    ]
                }
            }
    }

### Update Dockerfile

On the Dockerfile, we just need:

    FROM ubuntu:18.04

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y

    COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar /

    EXPOSE 59001

    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

### Saving

Finally,  we will need to go to command line and follow the usual steps:

    $git add .
    $git commit -m "Creating Jenkinsfile"
    $git push

(Everytime you change anything on any of your files, you will need to follow these steps)

### Build the pipeline

Now go to Jenkins and build a new pipeline for Part2.

Remember to check Console output in Jenkins if for some reason it fails to build

It is normal for something to be missing, and we might need several tries.

So after everything is built, we should have Jenkins with everything green on the last Build like this:

![img_1.png](img_1.png)

# DevOps Report CA5 - Alternative

## GitLab

### What is?

Gitlab is a very popular open source DevOps platform and a great alternative to Jenkins.

This tool is a web-based Git repository that provides free open and private repositories, issue-following capabilities, and wikis. 

It is a DevOps platform that allows professionals to perform all the tasks in a project.

## Gitlab vs Jenkins

### Price

Jenkins is open-source and available to all users, the only cost one could have would be the hosting expense of the server.

Gitlab on the other side have different packages you can select from, that go from 20 dollars a month to 100.

### Instalation

Both are very simple tool to set up.

The main difference is that Gitlab only supports Unix-based platforms, which means it does not support Windows or Mac.

To install Gitlab on Windows or Mac we need to go through a round-about process.

Regarding plugins, both offer the necessary plugins to support the user in every stage, but Jenkins is a much extensive bundle of plugins to offer.


### Other details

Although Gitlab offers a built-in Continuous Integration/ Continuous deployment feature, Jenkins also provides a easy way to set this up.

Regarding technical support, much of the Jenkins success is due to its community support. A dedicated technical support team does not exist for Jenkins, but there is documentation and much community help from installation to usage.

Gitlab has a dedicated technical team for paid users, and for free users and extensive documentation is available as well as community support.

Jenkins REST API supports XML, Python, and JSON, enabling you to extend its functionality. Gitlab offers a REST API for projects, groups, and standalone ones.

### Conclusion

Jenkins is heavily focused on continuous integration. And sicne it is open-source and completely free, and it comes as self-hosted and on-premise, it is favourable for startups and medium-sized businesses.

Gitlab outperforms Jenkins when it comes to version control and code collaboration.

Unlike Jenkins, where you can only monitor some branches, with Gitlab you can monitor all repository along with all branches. Which, in some situations, makes it a better option for big companies.

