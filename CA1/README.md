
---
# DevOps report CA1

## Implementation and analysis

### Config git
On Windows cmd run:

$ git config --global user.name "Your name"

$ git config --global user.email youremail@email.com 

### Set up new repository
On bitbucket, in Repositories tab, click "Create Repository".

### Clone to personal computer
Open Windows cmd and use "git clone" command. For example:

$ cd /pickyourdirectory

$ git clone https://github.com/spring-guides/tut-react-and-spring-data-rest

Now you are connected to your new repository and can start to working on it.

### Check status
To check the status of your git, just use the command "git status".

Here you can see what changes were done, and which one are committed.

In red we can see changes and in green we see changes already in commits.

### Create new folder named CA1 
Open the folder of your project and create a new one called "CA1"

Another alternative to create a new folder is through cmd, use the command:

$mkdir newfoldername

### Created README file
Run the command

$ git add README.adoc

### Create issue and resolve it
On bitbucket go to Issues, create a new one called "Create README file".

Double-check the issue number is 1 and go back to cmd.

Now let's push to the repository the changes, which is the creation of a README file.

Check status to make sure the change is to be committed. If it is use the command:

$git commit -m "Commit description (resolve #1)"

Check status once again. Now use:

$git push

Type in your app password from your repository and everything should have been saved.

### Add new column to table

Add the info on the Java files

Now on the command line move to the basic folder, inside the tut-react-and-spring-data-rest folder.

Now let's execute the application so we can see the table with the new info added.

Use the command:

$mvnw spring-boot:run

And finally open the following URL in your browser:

http://localhost:8080/


### Add and remove tags

There are many commands regarding tags, firstly check the command to see which tags you have:

$git tag

For more detailed info:

$git tag log

To create a new tag:

$git tag tagname

To delete a tag:

$git tag -d tagname

Push tag changes to repository:

$git push origins --tags


## Alternatives to git
### Mercurial
Both Git and Mercurial are an open-source version control tools, and although Git is more popular, there are a few differences between.

Git is a more complex, complete and harder to use tool, is preferred by large organizations, where Mercurial is more user-friendly and is mainly used on situations where the team managing the software is small.

Regarding the tools, Git is more flexible in the way to access versions, where the user has complete access to version history.
Mercury on the other hand, the user has permission to change last commit, but does not have to change version history.

Another impactful difference is the nonexistence of the staging area on Mercury tool.

I created an account on Mercurial, and cloned my project to try it out.

https://sourceforge.net/projects/devops-21-22-lmn-1211758/

I did a few commits and researched the commands for this tools. Most are almost identical, with the change the we have $hg at the beginning instead.



## DevOps Report - Part 2

### Branches

To check what branches you have and on which one you are working just use:

$git branch

This will get you a list of all the branches, and the one you are currently working will appear in bold.

### Create a new branch

To create a new branch called "testing", just use the command:

$git branch testing

Now you created a new branch and are currently working on it.

### Change between branches

To jump from one branch to another we just need to use the following command:

$git checkout master

Now we are back to the master branch and any changes, commits or push will be done on the master branch.

### Merge branches

In order to merge branches, let's switch back to the testing branch.

Now make some changes to the branch.

At this point we need to save the changes, so use the add command.

Use the git status to make sure the changes are green. If they are just use the commit command leaving a note and push.

Finally, we can merge the branches "testing" and "master".

Go to the master branch using:

$git checkout master

And now to merge we need to use:

$git merge testing

If there are no conflicts, the merge should be complete now. If there are any conflicts we will need to choose which part to save.

You can always do $git status to check if you have unmerged branches or conflicts to be dealt with. 

### Deleting a branch

Keeping things organized should always be a top priority, so if you do not need a branch anymore we should delete it. For that you can use:

$git branch -d testing

To confirm, do $git branch and make sure the deleted branch does not appear on the list anymore.

### Check commits done

It can be useful to check the last commits you did, so to get a list of the last commits done on all branches use:

$git branch -v







