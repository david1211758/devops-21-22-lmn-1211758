
---
# DevOps report CA3 - Part 1

## Create a virtual machine

### Install a Hypervisor

We will be using the Virtualbox, which is a free hypervisor that wuns on most operating systems.

Go to virtualbox.org and install the correct version for your computer.


### Create a new virtual machine

Open Virtualbox and click New.

Create a new virtual machine, using the specifications from the class.

Choose the type Linux with Ubuntu system.

Provide 2048MB of RAM memory and 10GB of dynamic disk space.


### Start the machine

Since the new machine has nothing on it, it does not start anything.

You should get an error of something:

    No bootable medium found.

Turn off the machine, so we can fix the issue.



### Emulate a CD ROM 

If we emulate a CD-ROM, the new machine can boot from that image of the CD.

That image comes in a type .iso file.

Go to https://help.ubuntu.com/community/Installation/MinimalCD and download the correct version of the minimal CD from ubuntu.

Now go back to virtualbox, and click on your new machine and then settings.

On storage tab, under "Controller: IDE", choose browse and then select the disk image you just downloaded.

Thick the box "Live CD/DVD", so the machine can boot from it.

Now try and start the machine again.


### Install Ubuntu

Now that everything is working, let's install Ubuntu.

The first image that shows up, gives you 4 options, click on the first one "Install".

Choose your preferences, from Language, location to keyboard lay out.

Select the partition of the disk that is going to be used.

Until Ubuntu is unpacking and installing the software.

Select no automatic updates.

Do not select any additional software programs.

Now that everything is installed, we need to remove the Disk Image, so the machine does not start via CD again.

Start the machine and Ubuntu should be running.

Log in with the credentials you set when installing the operating system.


### Set network adapters

Go to your machine settings, to the Network tab.

As you can see we only have the NAT adapter, which only allows us to connect to the internet.

We need to create a second adapter. So turn off the machine and on virtualbox go to File -> Host Network Manager.

Click on create, and it should automatically create a new one.

Go back to your machine settings and on network tab, select Adapter 2.

Enable that one and attach it to the adapter you created, a Host-only adapter.


### Continue the setup

After login once again, follow the steps:

Update the packages repositories

    $sudo apt update

Install the network tools

    $sudo apt install net-tools

Edit the network configuration file to setup the IP

    $sudo nano /etc/netplan/01-netcfg.yaml

This last one will open a text editor, make sure the text you see is as follows:

    network:
    version: 2
    renderer: networkd
    ethernets:
    enp0s3:
    dhcp4: yes
    enp0s8:
    addresses:
    -192.168.56.5/24

To save use CTRL + O, and to exist the text editor click CTRL + X.

Now apply these changes:

    $sudo netplan apply

Let's continue, install openssh-server so that we can use ssh to open secure terminal sessions to the
VM (from other hosts)

    $sudo apt install openssh-server

Enable password authentication for ssh

    $sudo nano /etc/ssh/sshd_config
    uncomment the line PasswordAuthentication yes
    $sudo service ssh restart

Install an ftp server so that we can use the FTP protocol to transfers files to/from
the VM (from other hosts)

    $sudo apt install vsftpd

Enable write access for vsftpd

    $sudo nano /etc/vsftpd.conf
    uncomment the line write_enable=YES
    $sudo service vsftpd restart

Now we have everything ready.


### Connect to the machine

Go to your own computer and open command line.

$ssh name@192.168.56.5

And use your credentials to login.

Now you are connected.

Now to install git you can use the command line from Windows instead.

    $sudo apt install git
    $sudo apt install openjdk-8-jdk-headless

### Clone the Spring Tutorial

We can now install and execute the Spring tutorial.

First, clone the application:

    $git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git

Change directory:

    $cd tut-react-and-spring-data-rest/basic

Build the application:

    $./mvnw spring-boot:run

Now go to your browser and go to the URL:

    $http://192.168.56.5:8080/






## Part 2

### Initialize a Vagrant project

First thing is first, we need to create a directory to initialize the project

    $mkdir vagrant-project-1
    $cd vagrant-project-1

Now to create a configuration file use:

    $vagrant init envimation/ubuntu-xenial

And finally start the VM

    $vagrant up

### Start a session in the new VM

To start a session in the new VM just use the following command:

    $vagrant ssh

To exit the VM:

    $exit

To stop the VM:

    $vagrant halt

To delete the VM:

    $vagrant destroy -f

### Clone another example

Change the directory and clone the project https://github.com/atb/vagrant-basic-example, using:

    $vagrant clone https://github.com/atb/vagrant-basic-example.git

Start that machine using the up command and after check it is running using:

    $vagrant status

If you stop the machine using the halt command and use the up command again, you can see not all the initialization was done again, since the configuration file was already used to config the machine, this second time the uo command was used only to turn on the machine.

### Clone example to use 2 virtual machines

Clone the following repository:

    $https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/

And now do

    $vagrant up

This might take a few minutes, but this one will create 2 virtual machines.

To make sure that is the case do:

    $vagrant status

As you can see on the README file on the new cloned repository, you can check the application two different ways:

    $http://localhost:8080/basic-0.0.1-SNAPSHOT/
    $http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

### Clone example to CA3 using your own repository

Copy the vagrant file to cd CA3 and then edit the file to clone your repository.

It should be on line 70 and delete the text there and add the git clone from your own repository. Make sure on the next line to add the correct directory as well.

Now on command line go to cd CA3 and do vagrant up.






#Alternative to VirtualBox - VMware

## VMware

VMware is a virtualization software. It is used to create and manage virtual machines, but it usually works best when running a single Virtual Machine.

They have a number of different virtualization products, and we can choose which one better fits for each use. For this analysis I will be focusing on the free version for non-comercial use.

VMware Workstation includes Type 2 hypervisors. Type 1 hypervisor replaces the underlying OS altogether, but VMware runs as an application on the desktop OS and lets users run a second OS on their main OS.

## VMware vs VirtualBox

Before going into technical details, one main difference between both software is on the licensing part.

VirtualBox is free and open-source, but VMware is free only for personal use, and the free version for personal and educational purposes have limited features, such as clones and snapshots.

Regarding a more technical view, the first difference worth of mention is that VMware only virtualizes hardware, on the other hand VirtualBox virtualizes hard and software.

Performance-wise, according to each one needs, the VMware machines run a lot faster, so it might be worth to investing on the VMware license.

This performance difference is not massive, or even noticeable on small projects, but on large-scale project it may have massive impact.

Since VMware is a more professional orientated tool, the interface is not the easier to get around to or instinctive. The user interface is more friendly on VirtualBox, with simpler screens.

## Vagrant

Although VirtualBox is the default provider when using Vagrant, we can easily set Vagrant to use VMserver as the default tool.



