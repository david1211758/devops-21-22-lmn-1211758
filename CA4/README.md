
---
# DevOps report CA4 - Part 1

## Containers with Docker

### Install Docker Desktop

Go to https://www.docker.com/products/docker-desktop/ and download the respective version for you.

### Create a docker file

Open notepad and create a file with the following in your desired directory:

    1# A basic apache server. Apache serves files under /var/www/html
    2 FROM ubuntu
    3
    4 ARG DEBIAN_FRONTEND=noninteractive
    5 RUN apt-get update
    6 RUN apt-get install -y apache2
    7 RUN apt-get clean && rm -rf /var/lib/apt/lists/*
    8
    9 ENV APACHE_RUN_USER www-data
    10 ENV APACHE_RUN_GROUP www-data
    11 ENV APACHE_LOG_DIR /var/log/apache2
    12 ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
    13 ENV APACHE_RUN_DIR /var/run/apache2
    14 ENV APACHE_LOCK_DIR /var/lock/apache2
    15 ENV APACHE_LOG_DIR /var/log/apache2
    16
    17 RUN mkdir -p $APACHE_RUN_DIR
    18 RUN mkdir -p $APACHE_LOCK_DIR
    19 RUN mkdir -p $APACHE_LOG_DIR
    20
    21 EXPOSE 80
    22
    23 # Copy files under ./public-html on the host to /var/www/html on the container
    24 COPY ./public-html/ /var/www/html

### Build a Docker

Open PowerShell on Windows as administrator and run the following command:

    $docker build .

### A few useful commands

    Docker information:
    $docker info
    List local docker images:
    $docker images
    Get an image from Docker Hub:
    $docker pull [image]
    $docker pull httpd:2.4
    Start a docker container:
    $docker run [image]
    $docker run –d httpd
    List running containers:
    $docker ps

### Run your image

Confirm the name of the image you created with:

    $docker images

And then

    $docker build -t my_image .
    $docker run -p 8080:80 -d my_image


## Create a Docker image and running container using the chat application from CA2

### Create the dockerfile and run the container

The first part is to say where from it will run and which version of system

    FROM ubuntu:18.04

Then we need to tell the container what we need to install, so it will work as intended.

For example, we need git and jdk.

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y
    RUN apt-get install git -y

Now when we start the container, it will automatically install all we need. So now we need to clone a git repository.

    RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/

Now we will define our workspace and define permissions on gradlew (just like we did on CA2)

    WORKDIR gradle_basic_demo/
    RUN chmod u+x gradlew
    RUN ./gradlew clean build

We will communicate from the host machine to the container via port, for example, 59001

    EXPOSE 59001

Finally, we need to open the chat server app, so we need to tell where it is and where to run it.

    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

At the end you should have something like:

    FROM ubuntu:18.04

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y
    RUN apt-get install git -y
    RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/

    WORKDIR gradle_basic_demo/
    RUN chmod u+x gradlew
    RUN ./gradlew clean build

    EXPOSE 59001

    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

Now use the build command using that file, so on the directory where the file is do:

    docker build -t imagename .
    docker run -p 59001:59001 -d imagename

To make sure everything is up and running, open your Docker Desktop. Check if you have one container running and with the message:

    The chat server is running...

Well done, you now built the chat server "inside" the Dockerfile.

### Explore the concept - Build the chat server in your host computer and copy the jar file to Dockerfile

Just to try something different, here we will build the server but only copying the jar file to the Dockerfile.

To do this we need to build a new image, with different commands on our Dockerfile.

We won't be needing git for this one, and then add a new command to copy the file.

I recommend copying the file to the same directory where the new Dockerfile is.

Now we have something as this: 

    FROM ubuntu:18.04

    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y

    COPY basic_demo-0.1.0.jar /

    EXPOSE 59001

    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

Now try to docker build and docker run as previously done and check is it works.

Remember: if you are using the same port (59001), you will need to stop the previous created container before running this one.

NOTE: my user on dockerhub is davidtg, here is my link https://hub.docker.com/u/davidtg


# DevOps report CA4 - Part 2

## Docker-compose

### What is?

Docker compose is a multi-containers tool from Docker. It helps you define and share multi-container applications.

### docker-compose.yml

The first thing we need to do is create a docker-compose.yml file.

To run the composer we will use the command $docker-compose up, this will trigger the file create to initiate and create everything we selected it to on the file.

Now create the docker compose file and add:

    version: '3'
    services:
    web:
    build: web
    ports:  
    - "8080:8080"  
    networks:
    default:
    ipv4_address: 192.168.56.10
    depends_on:
    - "db"
    db:
    build: db
    ports:
    - "8082:8082"
    - "9092:9092"
    volumes:
    - ./data:/usr/src/data-backup
    networks:
    default:
    ipv4_address: 192.168.56.11
    networks:
    default:
    ipam:
    driver: default
    config:
    -  subnet: 192.168.56.0/24

Now that we defined the containers to be created ("db" and "web"), and also defined all the network settings for each, including IP, communication ports  and root folder, we will need to a Dockerfile for each container.

Let's start with the "db" container, and create the following Dockerfile for it:

    FROM ubuntu

    RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless && \
    apt-get install unzip -y && \
    apt-get install wget -y

    RUN mkdir -p /usr/src/app

    WORKDIR /usr/src/app/

    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

    EXPOSE 8082
    EXPOSE 9092

    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

And finally the "web" container Dockerfile:

    FROM ubuntu:18.04
    RUN apt-get -y update && apt-get -y upgrade
    RUN apt-get -y install openjdk-11-jdk wget
    RUN mkdir /usr/local/tomcat
    RUN wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.78/bin/apache-tomcat-8.5.78.tar.gz  -O /tmp/tomcat.tar.gz
    RUN cd /tmp && tar xvfz tomcat.tar.gz
    RUN cp -Rv /tmp/apache-tomcat-8.5.78/* /usr/local/tomcat/

    RUN apt-get install sudo nano git nodejs npm -f -y

    RUN apt-get clean && rm -rf /var/lib/apt/lists/*

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://David1211758@bitbucket.org/david1211758/devops-21-22-lmn-1211758.git

    WORKDIR /tmp/build/devops-21-22-lmn-1211758/CA3/react-and-spring-data-rest-basic/

    RUN chmod u+x gradlew

    RUN ./gradlew clean build && cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -Rf /tmp/build/

    EXPOSE 8080

    CMD /usr/local/tomcat/bin/catalina.sh run

On this last one we had to add a couple of lines at the beginning so the container will manually install Tomcat.

Now we can visit the link and check if it is working.

    http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

Next step is to tag the image and push it to Docker hub.

    $docker login

    $docker imageName web davidtg/ca4part2_web_v1.1

    $docker push davidtg/ca4part2_web_v1.1

On this first one, I tagged the image I wanted (imageName, or you can search with Docker images for the one you want), and added my username (davidtg) followed by the name of the tag and then I pushed that tag.

Now we need to do it again for db.

    $docker login

    $docker imageName db davidtg/ca4part2_db_v1.1

    $docker push davidtg/ca4part2_db_v1.1

### Volumes

A volume is a shared folder between the host machine and the container.

In our case, we defined on our docker-compose file that ./data:/usr/src/data-backup is the shared folder.

Now let's access our container db and open the bash command line:

    $docker compose exec db bash

If we do

    $ls

We can see the files and folders, including the file we want to copy to our host machine: jpadb.mv.db

Now to copy that file to our host we just need to use the command cp:

    $cp jpadb.mv.db /usr/src/data-backup

In this case, if we did not save the file, after the container was shut down, we would have lost the file, since containers run in memory. Since we copied it to our host, the file is safe even after shutting down the container.

# Alternative - Kubernetes

Kubernetes is a portable, extensible, open source platform with the goal to manage containerized workloads and services, that facilitates declarative configuration and also automation.

Kubernetes is very extensible and automates many common operations. 

It addresses the need for enterprises to use container deployment. Kubernetes also aids in service discovery, load balancing and delivering reliability benefits. 

Regarding the community, Kubernetes has a vibrant open source community which correlates much co-building around the platform and contributes many compatible tools.


## Kubernetes vs Docker

Both services have a lot in common. Both are open cloud-native technologies, major cloud service providers in the world.

The major difference between them is that Docker is about packaging applications of containers in a single node and Kubernetes runs them in a cluster (a set of nodes).

Since these actions can be used for different things, they are often used together and not exclusively.

When used independently, normally smaller projects benefit more from using Docker and bigger companies usually opt to use Kubernetes, since they can support their maintenance.

Kubernetes is commonly used with Docker containers, but it can work with other types of containers.

## Details comparison 

| Parameters | Kubernetes | Docker  |
|:---  |:---- |:---|
| Developer | Google | Docker Inc |
| Launch | 2014 | 2013 | 
| Installation | Complex | Easy |
| Monitoring |	Built-in |  Needs third-party |  
| Cluster configuration | Simple |  Complex |
| Volumes |  Shared by all containers |  Shared by all containers in a Pod |
| Scaling | Slow | Fast |
| Load balancer | Manual | Auto |
| Containers limit | 300000 | 95000 |

## Conclusion

Docker and Kubernetes are technologies with different scopes. You can use Docker without Kubernetes and vice versa, however they work well together.

Docker strength is development, this is configuring, building and distributing containers using, for example, DockerHUb as image registry.

Kubernetes strength is operations, such as allowing the use of existing Docker containers while tackling the complexities of deployment, networking, scaling and monitoring.